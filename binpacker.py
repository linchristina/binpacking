# Christina Lin
# Using Python 2.7

"""

GT Nexus Python Coding Test

Bin packing problems are planning problems that are commonly studied in
operations research and that occur in many real life situations.  One form of
the bin packing problem is as follows.  You are given a catalog of bins, each
with a size and a cost.  You are also given a list of items that must be packed
into the bins.  A solution to the bin packing problem is then an assignment of
items to bins such that all items fit into a bin.  The cost of a solution is
the the sum of the costs of the bins used in the solution.  An optimal solution
is a solution that has least cost.

For example, I may have the following catalog of BinsTypes:

    >>> catalog = [
    ...     BinType(name="small",  size=50, cost=20),
    ...     BinType(name="medium", size=100, cost=25),
    ...     BinType(name="large",  size=200, cost=30),
    ...     BinType(name="xlarge", size=300, cost=35),
    ... ]

I also may have a list of Items:

    >>> items = [
    ...     Item(name="Widget 1",   size=40),
    ...     Item(name="Widget 2",   size=50),
    ...     Item(name="Sprocket 3", size=50),
    ... ]

A solution is then a packing list that groups items together into a bin type
such that the sum of the size of the items in each bin is less than the size of
each bin.  For example, one solution to the problem presented above may be:

    >>> solution1 = [
    ...     PackingList(bin_type=catalog[3],   items=items),
    ... ]

Here we pack all of the items into an "xlarge" bin.  This is a valid solution
since the sum of the size of all items is 140, and the xlarge bin has a size of
300.  The cost of this solution is 35 since that is the cost of the "xlarge"
bin.  Alternatively, another valid solution may be:

    >>> solution2 = [
    ...     PackingList(bin_type=catalog[0],   items=[items[0]]),
    ...     PackingList(bin_type=catalog[0],   items=[items[1]]),
    ...     PackingList(bin_type=catalog[0],   items=[items[2]]),
    ...     PackingList(bin_type=catalog[0],   items=[items[3]]),
    ... ]

Here we pack each item into its own "small" bin.  Your task will be to
implement a "BinPacker" class that accepts a catalog of bins and a list of
items as an argument to the constructor and implements 3 methods: solve, cost,
and validate.  "solve" should return a valid solution.  The solution doesn't
need to be optimal, but it should be valid.  "cost" should return the cost of a
given solution, and "validate" will check that a given solution is valid and
raise an exception if it isn't.  For example, we would expect the following to
work:

    >>> packer = BinPacker(catalog=catalog, items=items)
    >>> assert packer.cost(solution1) == 35
    >>> assert packer.cost(solution2) == 80
    >>> solution3 = packer.solve()
    >>> packer.validate(solution3) # doesn't raise exception

We have provided basic implementations of BinType, Item, PackingList, and a
shell of BinPacker.  However, it is up to you to complete the implementations.
We have also provided a "main" block at the end of the file so that you may
test your implementation by simply executing the module.  Please follow the
following rules:

    1. All work must be yours and yours alone.  You may use internet resources,
       but do not copy solutions or others work.

    2. Only add code in blocks clearly marked by comments for doing so.

    3. You may add methods to BinType, Item, and PackingList and BinPacker, but
       you may not edit the constructors.

    4. You may add imports and can use any features in the standard library,
       but do not add external dependencies.

    5. You may use either python 2.7 or 3+, but please indicate which you
       choose.

Your code will be evaluated on several dimensions including style and
correctness.  Optimality is not a requirement, but you should strive for as good
solution as you can find while ensure the solution is valid.  You should be
able to discuss and analyze the performance, optimality, and correctness of
your solution during follow up interviews.

Copyright 2015, GT Nexus. All rights reserved.

"""

from __future__ import print_function, division

############## YOUR CODE GOES HERE #################
# You may add imports
# from here to END CUSTOM CODE line


############## END CUSTOM CODE #####################

class BinType(object):
    """
    BinType is an object that defines a type of bin into which items may be
    packed.  Note that this simply specifies the size and cost of the bin does
    not function as the container for Items, you should use packing list for
    that.
    """

    def __init__(self, name, size, cost):
        self.name = name
        self.size = size
        self.cost = cost

    def __repr__(self):
        return "BinType({0}, {1}, {2})".format(self.name, self.size, self.cost)

    ############## YOUR CODE GOES HERE #################
    # You may add methods and properties for BinType
    # from here to END CUSTOM CODE line


    ############## END CUSTOM CODE #####################


class Item(object):
    """
    Item is an object that takes a name and size and represents a unique item
    that must be packed.
    """

    def __init__(self, name, size):
        self.name = name
        self.size = size

    def __repr__(self):
        return "Item({0}, {1})".format(self.name, self.size)

    ############## YOUR CODE GOES HERE #################
    # You may add methods and properties for Item
    # from here to END CUSTOM CODE line


    ############## END CUSTOM CODE #####################

class PackingList(object):
    """
    An object that takes BinType and optionally a list of items to pack into a
    bin of the given type.
    """

    def __init__(self, bin_type, items=None):
        self.bin_type = bin_type
        self.items = items

    def __repr__(self):
        return "PackingList({0}, {1})".format(self.bin_type, self.items)

    ############## YOUR CODE GOES HERE #################
    # You may add methods and properties for PackingList
    # from here to END CUSTOM CODE line

    # appends an item to the bin
    def append(self, item):
        self.items.append(item)

    ############## END CUSTOM CODE #####################

class BinPacker(object):
    """
    BinPacker should be able to generate solutions to bin packing problems as
    well as validate the solutions and evaluate their cost.
    """

    def __init__(self, catalog, items):
        self.catalog = catalog
        self.items = items

    def solve(self):
        """
        Solve should return a list of PackingList objects that indicate how all
        of the provided items should get packed.
        """
        ############## YOUR CODE GOES HERE #################
        # You should provide an implementation of solve
        # from here to END CUSTOM CODE line

        bin_space = 0      # remaining space in bin
        to_pack = 0         # remaining size of items to be packed
        total_size = 0      # total size of items to be packed
        for i in items:
            total_size += i.size

        list = []
        # check whether the total size of the items can fit entirely in one bin
        if total_size <= catalog[0].size:
            list = PackingList(catalog[0], items)
        elif total_size <= catalog[1].size:
            list = PackingList(catalog[1], items)
        elif total_size <= catalog[2].size:
            list = PackingList(catalog[2], items)
        else:
            # total size to be packed is larger than the largest bin
            # therefore start packing with the largest bin
            bin_space = catalog[2].size
            pack_list = PackingList(catalog[2], [])
            to_pack = total_size
            for i in items:
                if bin_space < i.size:
                    # no more space, start a new bin
                    # add packing list to list before starting a new bin
                    list.append(pack_list)
                    if to_pack <= catalog[0].size:
                        pack_list = PackingList(catalog[0], [])
                        bin_space = catalog[0].size

                    elif to_pack <= catalog[1].size:
                        pack_list = PackingList(catalog[1], [])
                        bin_space = catalog[1].size

                    else:
                        pack_list = PackingList(catalog[2], [])
                        bin_space = catalog[2].size

                pack_list.append(i)
                bin_space -= i.size
                to_pack -= i.size

            # add the last packing list to list
            list.append(pack_list)

        return list

        ############## END CUSTOM CODE #####################

    def cost(self, solution):
        """
        Cost will accept a list of PackingList objects and return a total cost
        for the solution.
        """
        ############## YOUR CODE GOES HERE #################
        # You should provide an implementation of cost
        # from here to END CUSTOM CODE line

        total_cost = 0
        for list in solution:
            total_cost += list.bin_type.cost

        return total_cost

        ############## END CUSTOM CODE #####################

    def validate(self, solution):
        """
        Cost will accept a list of PackingList objects and raise and exception
        if the any bins are overfilled or if any items aren't packed.
        """
        ############## YOUR CODE GOES HERE #################
        # You should provide an implementation of validate
        # from here to END CUSTOM CODE line

        total_items = 0
        for list in solution:
            total_size = 0
            total_items += len(list.items)
            for item in list.items:
                total_size += item.size
            # if the total size of the items is larger than its bin
            if total_size > list.bin_type.size:
                raise Exception("Bin is overfilled")

        if total_items != len(self.items):
            raise Exception("Not all items are packed")

        ############## END CUSTOM CODE #####################


    ############## YOUR CODE GOES HERE #################
    # You may add methods and properties for BinPacker
    # from here to END CUSTOM CODE line

    ############## END CUSTOM CODE #####################



if __name__ == "__main__":

    # create a bunch of items
    widgets = [Item(name="Widget_{0}".format(n), size=10) for n in range(10)]
    sprockets = [Item(name="Sprockets_{0}".format(n), size=12) for n in range(10)]
    items = widgets + sprockets


    # create the catalog
    catalog = [
        BinType(name="small",  size=20, cost=20),
        BinType(name="medium", size=60, cost=25),
        BinType(name="large",  size=110, cost=40),
    ]
    biggest_bin = catalog[-1]

    naive_solution = [
        PackingList(bin_type=biggest_bin, items=[item])
        for item in items
    ]

    binpacker = BinPacker(catalog=catalog, items=items)

    # validate naive solution
    try:
        binpacker.validate(naive_solution)
        print("PASSED: BinPacker correctly found the naive solution to be valid.")
    except Exception:
        print("FAILED: BinPacker incorrectly found the naive solution to be invalid.")

    # validate incomplte solution
    try:
        binpacker.validate(naive_solution[:10])
        print("FAILED: BinPacker incorrectly found an incomplete solution to be valid.")
    except Exception:
        print("PASSED: BinPacker correctly found an incomplete solution to be invalid.")

    # check cost
    if binpacker.cost(naive_solution) == 800:
        print("PASSED: BinPacker correctly computed the cost of the naive solution.")
    else:
        print("FAILED: BinPacker incorrectly computed the cost of the naive solution.")

    better_solution = binpacker.solve()

    try:
        binpacker.validate(better_solution)
        print("PASSED: BinPacker found a valid solution.")
    except Exception:
        print("FAILED: BinPacker found an invalid solution.")

    print("BinPacker's solution costs: {0}".format(binpacker.cost(better_solution)))


